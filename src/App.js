
import './App.css';
import React, { useEffect } from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Header from './components/header/header';
import { AppRoutes } from './AppRoutes/appRoutes';
import { useDispatch } from "react-redux"
import { useSelector } from 'react-redux'
import { getProductsAsync } from './redux/actions/products';

export default function App() {
   const dispatch = useDispatch()
   const products = useSelector((state) => state.products)

   useEffect(() => {
      dispatch(getProductsAsync())
   }, [dispatch]);

   useEffect(() => {
      document.title = 'Notebooks Store';
   }, []);

   return (
      <Router>
         <Header />
         {products?.length ?
            <div className='main-wrap'>
               <AppRoutes />
            </div> : <div>Loading...</div>}
      </Router >
   )
}



