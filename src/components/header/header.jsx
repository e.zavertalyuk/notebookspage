import React from "react";
import './header.scss';
import 'bootstrap/dist/css/bootstrap.css';
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import { useSelector } from 'react-redux'
export default function Header() {

   const { cartItems } = useSelector((state) => state.cart)
   const { favItems } = useSelector((state) => state.fav)
   const cartItemsLength = cartItems.reduce((acc, item) => {
      return acc + item.amount;
   }, 0);

   return (
      <>
         <div className="header">
            <NavLink className='linkItem' to="/">
               Home
            </NavLink>
            <NavLink to="/cart">
               <button type="button" className="btn btn-primary me-4">
                  Cart <span className="badge text-bg-dark">{cartItemsLength}</span>
               </button>
            </NavLink>
            <NavLink to="/favourite">
               <button type="button" className="btn text-bg-warning">
                  Favourites <span className="badge text-bg-dark">{favItems.length}</span>
               </button>
            </NavLink>
         </div>
      </>
   );
};

Header.propTypes = {
   count: PropTypes.number,
   countFav: PropTypes.number,
};

Header.defaultProps = {
   count: 0,
   countFav: 0,
};

