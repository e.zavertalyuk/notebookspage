import ReactPaginate from "react-paginate";
import './style.scss'
import { useEffect, useState } from "react";

const Pagination = ({ itemsPerPage, totalItems, onPageChange }) => {
   const [pageCount, setPageCount] = useState(0);
   useEffect(() => {
      setPageCount(Math.ceil(totalItems / itemsPerPage));
   }, [totalItems, itemsPerPage]);

   const handlePageClick = (selected) => {
      onPageChange(selected.selected);
   };

   return (
      <ReactPaginate
         className="pagination"
         breakLabel="..."
         nextLabel=">"
         pageRangeDisplayed={3}
         marginPagesDisplayed={2}
         pageCount={pageCount}
         onPageChange={handlePageClick}
         previousLabel="<"
         renderOnZeroPageCount={null}
      />
   );
};

export default Pagination;