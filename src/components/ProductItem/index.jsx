
import './style.scss'
import PropTypes from "prop-types";
import { useSelector } from "react-redux"
import { useDispatch } from "react-redux"
import { useContext } from 'react';
import { addToFavItems, removeFromFavItems } from "../../redux/actions/favItems";
import { toogleModal, cartModal } from '../../redux/actions/modal';
import { decreaseAmount, increaseAmount } from '../../redux/actions/cartItems';
import { selectItem } from '../../redux/actions/selectedItem';
import { CardView } from '../../context/cardsView';
export function ProductItem(props) {
   const { cardsView } = useContext(CardView)
   const dispatch = useDispatch()
   const products = useSelector((state) => state.products)
   const { favourite, item } = props;

   const favBtnClick = (e) => {
      const { id } = item;
      if (favourite) {
         dispatch(removeFromFavItems(id));
      } else {
         dispatch(addToFavItems(products, id));
      }
   };
   const handleClickAddToChart = (id) => {
      const item = products.find((item) => item.id === id);
      dispatch(selectItem(item))
      dispatch(cartModal())
      dispatch(toogleModal())
   };

   const handleRemoveToCart = (id) => {
      const item = products.find((item) => item.id === id);
      dispatch(selectItem(item))
      dispatch(toogleModal())
   };
   const { name, price, article, img, color } = props.item
   return (

      <div data-testid='itemCard' className={cardsView === 'cards' ? 'item' : 'itemRows'}>
         {props.delBtn ? <span onClick={() => handleRemoveToCart(item.id)} className="deleteItem">&#10006;</span> : null
         }
         <img className="itemPhoto" src={img} alt={name} width={200} height={150} />
         <p className={`item-info ${cardsView === 'cards' ? 'itemName' : 'itemNameRows'}`}>{name}</p>
         <p className="item-info itemPrice ">Price: <span className="priceSpan">{price} UAH</span></p>
         <p className="item-info itemaArticle">Article: {article}</p>
         <p className="item-info itemColor ">Color: {color}</p>

         {props.inCart ? <div className="item-info cartItem ">

            {item.amount === 1 ? <button onClick={() => handleRemoveToCart(item.id)} className='itemButtons'>-</button> :
               <button onClick={() => dispatch(decreaseAmount(item))} className='itemButtons'>-</button>}
            <span className='itemAmount'>{item.amount}</span>
            <button className='itemButtons plusBtn' onClick={() => dispatch(increaseAmount(item))}>+</button>
            <svg data-testid='favBtn' onClick={(e, id) => favBtnClick(e, id)} className={` ${cardsView === 'cards' ? "fav-btn" : 'favBtnRows'}`} xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24">
               <path fill={favourite ? '#FF6600' : '#000000'} d="M12,2.6L9.3,8.5l-6.1,0.6c-0.89,0.09-1.25,1.19-0.61,1.8L7.5,15l-2.2,6c-0.32,0.86,0.74,1.58,1.45,1.06L12,18.4l5.85,3.66c0.72,0.45,1.77-0.2,1.45-1.06l-2.2-6l4.98-4.4c0.64-0.57,0.28-1.67-0.61-1.8l-6.1-0.6L12,2.6z" />
               <path d="M0,0h24v24H0V0z" fill="none" />
            </svg>
         </div> :

            <div className="btn-wrap">
               <button data-testid="addToCart" onClick={() => handleClickAddToChart(item.id)} className="cart-btn">Add to cart</button>
               <svg data-testid='favBtn' onClick={(e, id) => favBtnClick(e, id)} className={cardsView === 'cards' ? "fav-btn" : 'favBtnRows'} xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24">
                  <path fill={favourite ? '#FF6600' : '#000000'} d="M12,2.6L9.3,8.5l-6.1,0.6c-0.89,0.09-1.25,1.19-0.61,1.8L7.5,15l-2.2,6c-0.32,0.86,0.74,1.58,1.45,1.06L12,18.4l5.85,3.66c0.72,0.45,1.77-0.2,1.45-1.06l-2.2-6l4.98-4.4c0.64-0.57,0.28-1.67-0.61-1.8l-6.1-0.6L12,2.6z" />
                  <path d="M0,0h24v24H0V0z" fill="none" />
               </svg>
            </div>}
      </div >
   )
}
ProductItem.propTypes = {
   addToFavClick: PropTypes.func,
   removeFromFavClick: PropTypes.func,
   onClick: PropTypes.func,
   item: PropTypes.object,
   favourite: PropTypes.bool,
};
ProductItem.defaultProps = {
   items: {},
};
export default ProductItem