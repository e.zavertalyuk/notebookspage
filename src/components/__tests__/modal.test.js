import Modal from "../modal";
import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { modalTypes } from "../../redux/types";
const mockStore = configureStore();
let store;
let modalProps;
beforeEach(() => {
   modalProps = {
      header: 'Test header',
      closeButton: true,
      text: 'Test text',
   }
   store = mockStore({
      selectItem: {
         selectedItem: {
            id: '1',
            name: 'Acer Aspire E5',
            price: '10000',
            article: '773588',
            img: 'img/notebook1.webp',
            color: 'black'
         }
      },
      modal: {
         openModal: true,
         modalInCart: false,
      },
   });
   cleanup();
});

test('snapshot modal', () => {
   const tree = renderer.create(
      <Provider store={store}>
         <Modal {...modalProps} />
      </Provider>
   ).toJSON();
   expect(tree).toMatchSnapshot()
})

test('should modal render', () => {
   render(<Provider store={store}>
      <Modal {...modalProps} />
   </Provider>)
   expect(screen.getByTestId('modal')).toBeInTheDocument();
   expect(screen.getByTestId('modal')).toHaveTextContent('Test header');
   expect(screen.getByTestId('modal')).toHaveTextContent('Test text');
})
test("testing modal closing", () => {
   render(
      <Provider store={store}>
         <Modal />
      </Provider>
   );
   const modalBg = screen.getByTestId("modalBg");
   const closeBtn = screen.getByTestId("close-modal");
   expect(modalBg).toBeInTheDocument();
   fireEvent.click(modalBg);
   fireEvent.click(closeBtn);
   const actions = store.getActions();
   expect(actions).toContainEqual({
      type: modalTypes.TOOGLE_MODAL,
   });
});


