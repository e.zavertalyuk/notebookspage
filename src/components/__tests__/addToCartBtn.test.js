import { render, screen, cleanup } from "@testing-library/react";
import ProductItem from "../ProductItem";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
const mockStore = configureStore();
let itemProps;
let store;
beforeEach(() => {
   itemProps = {
      item: {
         name: 'Lenovo',
         price: '10000',
         article: '494885',
         img: '../../../public/img/notebook1.webp',
         color: 'black'
      }

   }
   store = mockStore({
      modal: {
         openModal: false,
         modalInCart: false,
      },
   });
   cleanup();
});
test('should add btn render', () => {
   render(<Provider store={store}>
      <ProductItem {...itemProps} />
   </Provider>)
   expect(screen.getByTestId('favBtn')).toBeInTheDocument();
})



