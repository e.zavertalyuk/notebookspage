import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import ProductItem from "../ProductItem";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
const mockStore = configureStore();
let itemProps;
let store;
beforeEach(() => {
   itemProps = {
      item: {
         id: 1,
         name: 'Lenovo',
         price: '10000',
         article: '494885',
         img: '../../../public/img/notebook1.webp',
         color: 'black'
      }

   }
   store = mockStore({
      modal: {
         openModal: false,
         modalInCart: false,
      },
   });
   cleanup();
});
test('should fav btn render', () => {
   render(<Provider store={store}>
      <ProductItem {...itemProps} />
   </Provider>)
   expect(screen.getByTestId('favBtn')).toBeInTheDocument();
})

test('should add to fav', () => {
   render(<Provider store={store}>
      <ProductItem {...itemProps} />
   </Provider>)
   expect(screen.getByTestId('favBtn')).toBeInTheDocument();
   const favBtn = screen.getByTestId("favBtn");
   fireEvent.click(favBtn);
   const actions = store.getActions();
   expect(actions).toEqual([{
      "payload": { "id": 1, "products": undefined },
      "type": "ADD_TO_FAV"
   }]);
})
