import Header from "../header/header";
import { render, screen, cleanup } from "@testing-library/react";
import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import configureStore from "redux-mock-store";
const mockStore = configureStore();
let store;
beforeEach(() => {
   store = mockStore({
      cart: {
         cartItems: [
            {
               id: '2',
               name: 'Lenovo IdeaPad L3 ',
               price: '20000',
               article: '234567',
               img: 'img/notebook2.webp',
               color: 'white',
               amount: 2
            },
         ]
      },
      fav: {
         favItems: [
            {
               id: '3',
               name: 'MacBook Pro',
               price: '30000',
               article: '177358',
               img: 'img/notebook3.webp',
               color: 'black',
               amount: 1
            },
         ]
      },
   });
   cleanup();
});

test('snapshot modal', () => {
   const tree = renderer.create(
      <Provider store={store}>
         <MemoryRouter>
            <Header />
         </MemoryRouter>
      </Provider>
   ).toJSON();
   expect(tree).toMatchSnapshot()
})