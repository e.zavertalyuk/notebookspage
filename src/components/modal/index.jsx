
import PropTypes from "prop-types";
import './modal.scss';
import { useDispatch, useSelector } from "react-redux";
import { toogleModal, cartModal } from "../../redux/actions/modal";
import { addToCartItems, removeFromCartItems } from "../../redux/actions/cartItems";
import { removeItem } from "../../redux/actions/selectedItem";
export default function Modal({ closeButton, header, text, actions, imgPath }) {
   const dispatch = useDispatch()
   const { modalInCart } = useSelector((state) => state.modal)
   const { selectedItem } = useSelector((state) => state.selectItem)
   const handleClickModalInCart = (e, id) => {
      if (
         e.target.className === "back" ||
         e.target.classList.contains("cancel") ||
         e.target.classList.contains("close-button")
      ) {
         if (modalInCart) {
            dispatch(cartModal());
         }
         dispatch(toogleModal());

      } else if (e.target.classList.contains("confirm")) {
         modalInCart ?
            dispatch(addToCartItems(selectedItem)) :
            dispatch(removeFromCartItems(selectedItem));
         dispatch(toogleModal())
         if (modalInCart) {
            dispatch(cartModal())
         }
         dispatch(removeItem())
      }
   }

   return (
      <>
         <div data-testid="modalBg" className="back" onClick={handleClickModalInCart}>
            <div data-testid="modal" className='modalBuy'>
               <h1 style={{ paddingLeft: '25px' }} className="modalHeader">
                  {header}
                  {closeButton && <button data-testid="close-modal" className="close-button">✖</button>}
               </h1>
               <img style={{ marginTop: '10px', borderRadius: '7px' }} src={imgPath} alt={imgPath} width={200} height={150} />
               <p className="text">{text}</p>
               {actions}
            </div>
         </div>
      </>
   );
};

Modal.propTypes = {
   onClick: PropTypes.func,
   closeButton: PropTypes.bool,
   header: PropTypes.string,
   text: PropTypes.string,
   actions: PropTypes.element,
};

Modal.defaultProps = {
   text: '',
   closeButton: true,
};
