import './home.scss';
import Modal from '../components/modal';
import ItemWrapper from '../components/ItemWrapper';
import Pagination from '../components/Pagination';
import { useSelector } from 'react-redux'
import { useContext, useState, useEffect } from 'react';
import { CardView } from '../context/cardsView';

export default function Home() {
   const { openModal } = useSelector((state) => state.modal)
   const products = useSelector((state) => state.products)
   const { selectedItem } = useSelector((state) => state.selectItem)
   const { cardsView, setCardsView } = useContext(CardView)
   const [noteList, setNoteList] = useState(products)
   const [searchTerm, setSearchTerm] = useState('')
   const [currentPage, setCurrentPage] = useState(0);
   const itemsPerPage = 6;

   const filteredProducts = products.filter(({ name }) =>
      name.toLowerCase().includes(searchTerm.toLowerCase())
   );

   const handlePageChange = (selected) => {
      if (selected >= Math.ceil(filteredProducts.length / itemsPerPage)) {
         setCurrentPage(Math.max(0, Math.ceil(filteredProducts.length / itemsPerPage) - 1));
      } else {
         setCurrentPage(selected);
      }
   };

   const filterNotes = (searchText, listOfNote) => {
      if (!searchText) {
         return listOfNote
      }
      return listOfNote.filter(({ name }) =>
         name.toLowerCase().includes(searchText.toLowerCase())
      )
   }
   useEffect(() => {
      setCurrentPage(0);
   }, [searchTerm]);

   useEffect(() => {
      const debounce = setTimeout(() => {
         setNoteList((prevNoteList) => filterNotes(searchTerm, products));
      }, 100);
      return () => clearTimeout(debounce);
   }, [searchTerm, products]);

   useEffect(() => {
      if (searchTerm === '') {
         setCurrentPage(0);
         setNoteList(products);
      }
   }, [searchTerm, noteList, products]);




   return (
      <div className='main-wrap'>
         {
            openModal ? <Modal closeButton
               header={"Добавить выбранный товар в корзину?"}
               text={` Цена товара ${selectedItem.price} UAH`}
               imgPath={selectedItem.img}
               actions={
                  <div className='btn-wrap'>
                     <button className="modal__content__btn confirm">Confirm</button>
                     <button className="modal__content__btn cancel">Cancel</button>
                  </div>
               }
            /> : null
         }
         <div className='changeParams'>
            <div className="btn-group" role="group" aria-label="Basic radio toggle button group">
               <input
                  type="radio"
                  className="btn-check"
                  name="btnradio"
                  id="btnradio1"
                  autoComplete='off'
                  checked={cardsView === 'cards'}
                  onChange={() => setCardsView('cards')}
               />
               <label style={{ zIndex: 0 }} htmlFor='btnradio1' className="btn btn-outline-primary">
                  Cards
               </label>

               <input
                  type="radio"
                  className="btn-check"
                  name="btnradio"
                  id="btnradio2"
                  autoComplete='off'
                  checked={cardsView === 'rows'}
                  onChange={() => setCardsView('rows')}
               />
               <label style={{ zIndex: 0 }} htmlFor='btnradio2' className="btn btn-outline-primary">
                  Rows
               </label>

            </div>
            <div style={{ position: 'relative' }} className="mb-3 inputWrap">
               <svg
                  className='searchIcon'
                  height={512}
                  viewBox="0 0 512 512"
                  width={512}
                  xmlns="http://www.w3.org/2000/svg"
               >
                  <title />
                  <path
                     d="M221.09,64A157.09,157.09,0,1,0,378.18,221.09,157.1,157.1,0,0,0,221.09,64Z"
                     style={{
                        fill: "none",
                        stroke: "#000",
                        strokeMiterlimit: 10,
                        strokeWidth: "32px"
                     }}
                  />
                  <line
                     style={{
                        fill: "none",
                        stroke: "#000",
                        strokeLinecap: "round",
                        strokeMiterlimit: 10,
                        strokeWidth: "32px"
                     }}
                     x1="338.29"
                     x2="448"
                     y1="338.29"
                     y2="448"
                  />
               </svg>


               <input className='searchInput' type="text" value={searchTerm} autoFocus autoComplete='off'
                  placeholder='Поиск товаров...'
                  onChange={(e) => setSearchTerm(e.target.value)}
               />
            </div>
         </div>

         <ItemWrapper
            items={filteredProducts.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage)}
         />
         <Pagination
            itemsPerPage={itemsPerPage}
            totalItems={filteredProducts.length}
            onPageChange={handlePageChange}
         />
      </div >
   )
}


