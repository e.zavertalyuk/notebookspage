import { useSelector } from "react-redux"
import ItemWrapper from "../components/ItemWrapper"
import Modal from "../components/modal"
import Form from "../components/form"
import './home.scss'

export default function CartItems(props) {

   const { cartItems } = useSelector((state) => state.cart)
   const { openModal } = useSelector((state) => state.modal)
   // const { modalInCart } = useSelector((state) => state.modal)
   const { selectedItem } = useSelector((state) => state.selectItem)
   return (
      <>
         <div>
            {
               openModal ? <Modal closeButton
                  header={"Вы желаете удалить товар из корзины?"}
                  text={"Подтвердите действие!"}
                  imgPath={selectedItem.img}
                  actions={
                     <div className='btn-wrap'>
                        <button className="modal__content__btn confirm">Confirm</button>
                        <button className="modal__content__btn cancel">Cancel</button>
                     </div>
                  }
               /> : null
            }

            {cartItems.length === 0 ? <div className="nothingAdd">Nothing has been added</div> :
               <>
                  <h2 className="titleText">Our cart:</h2>
                  <div className="cartWrap">
                     <ItemWrapper
                        items={cartItems}
                        delBtn
                        inCart
                     />
                     <Form />
                  </div>
               </>}
         </div >
      </>
   )
}