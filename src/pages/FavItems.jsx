import ItemWrapper from "../components/ItemWrapper"
import Modal from "../components/modal"
import { useSelector } from "react-redux"

export default function FavItems(props) {
   const { favItems } = useSelector((state) => state.fav)
   const { openModal } = useSelector((state) => state.modal)
   const { selectedItem } = useSelector((state) => state.selectItem)

   return (
      <>
         {
            openModal ? <Modal closeButton
               header={"Добавить выбранный товар в корзину?"}
               imgPath={selectedItem.img}
               text={` Цена товара ${selectedItem.price} UAH`}
               actions={
                  <div className='btn-wrap'>
                     <button className="modal__content__btn confirm">Confirm</button>
                     <button className="modal__content__btn cancel">Cancel</button>
                  </div>
               }
            /> : null
         }
         {favItems.length === 0 ? <div className="nothingAdd">Nothing has been added</div> : <>
            <h2 className="titleText">Our Favourites:</h2>

            <ItemWrapper
               items={favItems}
            />
         </>}
      </>
   )
}