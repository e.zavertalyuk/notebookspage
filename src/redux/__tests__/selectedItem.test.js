
import { selectItem } from "../reducers/selectedItem";
import { selectedItemTypes } from "../types";
describe('selectItem', () => {
   const initialState = {
      selectedItem: null
   };

   it('should handle SELECT_ITEM', () => {
      const selectedItem = {
         id: '1',
         name: 'Product 1',
         price: 10
      };
      const action = {
         type: selectedItemTypes.SELECT_ITEM,
         payload: selectedItem
      };

      const state = selectItem(initialState, action);

      expect(state).toEqual({
         selectedItem: selectedItem
      });
   });

   it('should handle REMOVE_ITEM', () => {
      const initialState = {
         selectedItem: {
            id: '1',
            name: 'Product 1',
            price: 10
         }
      };
      const action = {
         type: selectedItemTypes.REMOVE_ITEM
      };

      const state = selectItem(initialState, action);

      expect(state).toEqual({
         selectedItem: null
      });
   });
});
