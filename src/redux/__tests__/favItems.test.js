
import { favReducer } from "../reducers/favItems";
import { favItemsTypes } from "../types";
describe('favReducer', () => {
   const initialState = {
      favItems: []
   };

   test('should handle ADD_TO_FAV', () => {
      const existingItem = {
         id: '1',
         name: 'Product 1',
         price: 10
      };
      const products = [existingItem];
      const action = {
         type: favItemsTypes.ADD_TO_FAV,
         payload: {
            id: '1',
            products: products
         }
      };

      const state = favReducer(initialState, action);

      expect(state).toEqual({
         favItems: [existingItem]
      });
   });

   test('should handle DEL_FROM_FAV', () => {
      const existingItems = [
         {
            id: '1',
            name: 'Product 1',
            price: 10
         },
         {
            id: '2',
            name: 'Product 2',
            price: 20
         }
      ];
      const initialState = {
         favItems: existingItems
      };
      const action = {
         type: favItemsTypes.DEL_FROM_FAV,
         payload: '1'
      };

      const state = favReducer(initialState, action);

      expect(state).toEqual({
         favItems: [
            {
               id: '2',
               name: 'Product 2',
               price: 20
            }
         ]
      });
   });
});
