
import { modalReducer } from '../reducers/modal';
import { modalTypes } from '../types';
describe('modalReducer', () => {
   test('should return the initial state', () => {
      expect(modalReducer(undefined, {})).toEqual({
         openModal: false,
         modalInCart: false
      });
   });

   test('should handle TOGGLE_MODAL action', () => {
      const initialState = {
         openModal: false,
         modalInCart: false
      };

      const action = {
         type: modalTypes.TOOGLE_MODAL
      };

      expect(modalReducer(initialState, action)).toEqual({
         openModal: true,
         modalInCart: false
      });
   });

   test('should handle CART_MODAL action', () => {
      const initialState = {
         openModal: false,
         modalInCart: false
      };

      const action = {
         type: modalTypes.CART_MODAL
      };

      expect(modalReducer(initialState, action)).toEqual({
         openModal: false,
         modalInCart: true
      });
   });
});
