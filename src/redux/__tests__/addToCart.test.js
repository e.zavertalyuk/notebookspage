
import { cartReducer } from "../reducers/cartItems";
import { cartItemsTypes } from "../types";
describe('cartReducer', () => {
   const initialState = {
      cartItems: []
   };

   test('should handle ADD_TO_CART', () => {
      const itemToAdd = {
         id: '1',
         name: 'Product 1',
         price: 10,
         amount: 1
      };
      const action = {
         type: cartItemsTypes.ADD_TO_CART,
         payload: itemToAdd
      };

      const state = cartReducer(initialState, action);

      expect(state).toEqual({
         cartItems: [itemToAdd]
      });
   });

   test('should handle DECREASE_AMOUNT', () => {
      const existingItem = {
         id: '1',
         name: 'Product 1',
         price: 10,
         amount: 2
      };
      const initialState = {
         cartItems: [existingItem]
      };
      const action = {
         type: cartItemsTypes.DECREASE_AMOUNT,
         payload: existingItem
      };

      const state = cartReducer(initialState, action);

      expect(state).toEqual({
         cartItems: [{
            ...existingItem,
            amount: 1
         }]
      });
   });

   test('should handle INCREASE_AMOUNT', () => {
      const existingItem = {
         id: '1',
         name: 'Product 1',
         price: 10,
         amount: 2
      };
      const initialState = {
         cartItems: [existingItem]
      };
      const action = {
         type: cartItemsTypes.INCREASE_AMOUNT,
         payload: existingItem
      };

      const state = cartReducer(initialState, action);

      expect(state).toEqual({
         cartItems: [{
            ...existingItem,
            amount: 3
         }]
      });
   });

   test('should handle DEL_FROM_CART', () => {
      const existingItem = {
         id: '1',
         name: 'Product 1',
         price: 10,
         amount: 2
      };
      const initialState = {
         cartItems: [existingItem]
      };
      const action = {
         type: cartItemsTypes.DEL_FROM_CART,
         payload: existingItem
      };

      const state = cartReducer(initialState, action);

      expect(state).toEqual({
         cartItems: []
      });
   });

   test('should handle CLEAR_CART', () => {
      const existingItems = [
         {
            id: '1',
            name: 'Product 1',
            price: 10,
            amount: 2
         },
         {
            id: '2',
            name: 'Product 2',
            price: 20,
            amount: 1
         }
      ];
      const initialState = {
         cartItems: existingItems
      };
      const action = {
         type: cartItemsTypes.CLEAR_CART
      };

      const state = cartReducer(initialState, action);

      expect(state).toEqual({
         cartItems: []
      });
   });
});
