import { cartItemsTypes } from "../types";

const initialCartItems = localStorage.getItem("cartItems");
const initialState = {
   cartItems: initialCartItems ? JSON.parse(initialCartItems) : []
}

export function cartReducer(state = initialState, action) {
   switch (action.type) {
      case cartItemsTypes.ADD_TO_CART:
         action.payload.amount = 1;
         const itemToAdd = state.cartItems.find((item) => item.id === action.payload.id);
         if (itemToAdd) {
            itemToAdd.amount++;
            const filteredCart = state.cartItems.filter((item) => item.id !== itemToAdd.id)
            const itemsArr = [...filteredCart, itemToAdd]
            localStorage.setItem("cartItems", JSON.stringify(itemsArr));
            return {
               cartItems: [...filteredCart, itemToAdd]
            }
         } else {
            localStorage.setItem("cartItems", JSON.stringify([...state.cartItems, action.payload]));
            return {
               cartItems: [...state.cartItems, { ...action.payload, amount: 1 }]
            }
         }

      case cartItemsTypes.DECREASE_AMOUNT:
         const itemDecrease = action.payload
         itemDecrease.amount--;
         localStorage.setItem("cartItems", JSON.stringify(state.cartItems));

         return {
            cartItems: [...state.cartItems]
         }
      case cartItemsTypes.INCREASE_AMOUNT:
         const itemIncrease = action.payload
         itemIncrease.amount++;
         localStorage.setItem("cartItems", JSON.stringify(state.cartItems));
         return {
            cartItems: [...state.cartItems]
         }

      case cartItemsTypes.DEL_FROM_CART:
         const deletingElIndex = state.cartItems.findIndex((item) => item.id === action.payload.id);
         const newCartArr = state.cartItems.filter((item, index) => index !== deletingElIndex);
         localStorage.setItem("cartItems", JSON.stringify(newCartArr));
         return {
            ...state,
            cartItems: newCartArr
         };
      case cartItemsTypes.CLEAR_CART:
         console.log(console.log('Данные для отправки:', action.payload, 'Купленные товары:', state.cartItems));
         localStorage.removeItem("cartItems");
         return {
            ...state,
            cartItems: []
         }
      default:
         return state
   }
}
