import { favItemsTypes } from "../types";

const initialFavItems = localStorage.getItem("favItems");
const initialState = {
   favItems: initialFavItems ? JSON.parse(initialFavItems) : []
}

export function favReducer(state = initialState, action) {
   switch (action.type) {
      case favItemsTypes.ADD_TO_FAV:
         const item = action.payload.products.find((item) => item.id === action.payload.id);
         const favItemsArr = [...state.favItems, item]
         localStorage.setItem("favItems", JSON.stringify(favItemsArr));
         return {
            ...state,
            favItems: favItemsArr,
         }
      case favItemsTypes.DEL_FROM_FAV:
         const newFavArr = state.favItems.filter((item) => item.id !== action.payload);
         localStorage.setItem("favItems", JSON.stringify(newFavArr));
         return {
            ...state,
            favItems: newFavArr
         };
      default:
         return state
   }
}
