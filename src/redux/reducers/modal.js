import { modalTypes } from "../types";

const initialState = {
   openModal: false,
   modalInCart: false
}

export function modalReducer(state = initialState, action) {
   switch (action.type) {
      case modalTypes.TOOGLE_MODAL:
         return {
            ...state,
            openModal: !state.openModal
         }
      case modalTypes.CART_MODAL:
         return {
            ...state,
            modalInCart: !state.modalInCart
         }

      default:
         return state
   }
}
