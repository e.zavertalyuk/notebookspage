import { productTypes } from "../types";
const initialState = {
   products: []
}

export function productsReducer(state = initialState, action) {
   switch (action.type) {
      case productTypes.FILL_PRODUCTS:
         return state.products = action.payload

      default:
         return state
   }
}