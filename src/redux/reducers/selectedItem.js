import { selectedItemTypes } from "../types";
const initialState = {
   selectedItem: null
}

export function selectItem(state = initialState, action) {
   switch (action.type) {
      case selectedItemTypes.SELECT_ITEM:
         return {
            ...state,
            selectedItem: action.payload
         }
      case selectedItemTypes.REMOVE_ITEM:
         return {
            ...state,
            selectedItem: null
         }
      default:
         return state
   }
}