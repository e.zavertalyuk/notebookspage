import { selectedItemTypes } from "../types";


export function selectItem(item) {
   return {
      type: selectedItemTypes.SELECT_ITEM,
      payload: item
   }
}

export function removeItem() {
   return {
      type: selectedItemTypes.REMOVE_ITEM
   }
}