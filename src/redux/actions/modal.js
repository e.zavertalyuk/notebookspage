import { modalTypes } from "../types";

export function toogleModal() {
   return {
      type: modalTypes.TOOGLE_MODAL
   }
}
export function cartModal() {
   return {
      type: modalTypes.CART_MODAL
   }
}