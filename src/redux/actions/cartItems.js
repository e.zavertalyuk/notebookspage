
import { cartItemsTypes } from "../types";

export function addToCartItems(item) {
   return {
      type: cartItemsTypes.ADD_TO_CART,
      payload: item,
   }
}
export function removeFromCartItems(item) {
   return {
      type: cartItemsTypes.DEL_FROM_CART,
      payload: item,
   }
}
export function clearCart(values) {
   return {
      type: cartItemsTypes.CLEAR_CART,
      payload: values
   }
}
export function decreaseAmount(item) {
   return {
      type: cartItemsTypes.DECREASE_AMOUNT,
      payload: item,
   }
};
export function increaseAmount(item) {
   return {
      type: cartItemsTypes.INCREASE_AMOUNT,
      payload: item,
   }
};