import { productTypes } from "../types";

export function fillProducts(data) {
   return {
      type: productTypes.FILL_PRODUCTS,
      payload: data,
   }
}

export function getProductsAsync() {
   return async function (dispatch) {
      const data = await fetch("/mock/items.json").then((res) => res.json())
      dispatch(fillProducts(data))
   }
}