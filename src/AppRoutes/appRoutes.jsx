import { Route, Routes } from "react-router-dom";
import Home from "../pages/Home";
import FavItems from "../pages/FavItems";
import CartItems from "../pages/CartItems";
import { CardView } from "../context/cardsView";
import { useState } from "react";
export function AppRoutes(props) {
   const [cardsView, setCardsView] = useState('cards')
   return (
      <CardView.Provider value={{ cardsView, setCardsView }}>
         <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/cart" element={<CartItems />} />
            <Route path="/favourite" element={<FavItems />} />
         </Routes>
      </CardView.Provider>
   )
}